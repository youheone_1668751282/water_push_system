<?php
/**
 * Created by PhpStorm.
 * User: rain1
 * Date: 2016/3/17
 * Time: 11:05
 */

namespace Network;

use Monolog\Logger;
use Server\MegaWechatServer;
use Swoole\Protocol\Base;
use Swoole\Queue\FileQueue;
use Swoole\Util\Config;
use Swoole\Util\Log;
use Wechat\Template;
use Wechat\WechatApi;
use Wechat\WechatTemplateModel;

class MegaWechatProtocol extends Base
{
    private $taskWorkerNum;
    /** @var FileQueue  */
    private $queue;
    /** @var WechatApi */
    private $wechatApi;
    /** @var Logger */
    private $logger;

    public function init()
    {
        parent::init();
        $this->setCodecFactory(new MegaWechatCodecFactory());
        //需要配置多个公众号小程序
        //$this->wechatApi = new WechatApi(Config::get('wechat.app_id'), Config::get('wechat.app_secret'), Config::get('wechat.token'));
        $this->logger = Log::getLogger();
    }

    /**
     * @param $server \swoole_server
     * @param $workerId
     */
    function onStart(\swoole_server $server, $workerId)
    {
        // worker进程
        if (!$server->taskworker) {
            $filePath = Config::get('server.queue_file_path') . '/task-' . $workerId;
            try
            {
                $this->queue = new FileQueue($filePath);
            }
            catch (\Exception $e)
            {
                var_dump($e->getMessage());
                $this->logger->warning($e->getMessage());
                $this->server->shutdown();
            }
            $this->taskWorkerNum = $this->server->setting['task_worker_num'];

            // 定时检测队列是否有未处理的数据
            $server->tick(10000, function() use ($server) {
                if (MegaWechatServer::$taskActiveNum->get() < $this->taskWorkerNum) {
                    if (($message = $this->queue->pop()) !== null) {
                        MegaWechatServer::$taskActiveNum->add(1);
                        $server->task($message);
                    }
                }
            });
        }
    }

    function onConnect(\swoole_server $server, $fd, $from_id)
    {
        $this->logger->info("Client@[{$fd}:{$from_id}] connect");
    }

    function onReceive(\swoole_server $server, $fd, $from_id, $data)
    {
        try {
            $command = $this->decode($data, $fd);
            if ($command instanceof SendCommand) {
                $this->logger->info(__LINE__ . " SendCommand {$command->toString()}");
                // 若微信模板消息存在，则放入队列
                if (MegaWechatServer::$templateTable->exist($command->getKey())) {
                    $this->queue->push(serialize($command));
                } else {
                    $message = new BooleanCommand(HttpStatus::BAD_REQUEST, 'template key not exists', $command->getOpaque());
                    $server->send($command->getFd(), $this->encode($message));
                }
            } else if ($command instanceof PushCommand) {
                $this->logger->info(__LINE__ . " PushCommand {$command->toString()}");
                if (MegaWechatServer::$templateTable->exist($command->getKey())) {
                    $this->queue->push(serialize($command));
                    $message = new BooleanCommand(HttpStatus::SUCCESS, null, $command->getOpaque());
                    $server->send($fd, $this->encode($message));
                } else {
                    $message = new BooleanCommand(HttpStatus::BAD_REQUEST, 'template key not exists', $command->getOpaque());
                    $server->send($fd, $this->encode($message));
                }
            } else if ($command instanceof SetTableCommand) {
                try {
                    $this->logger->info(__LINE__ . " SetTableCommand {$command->toString()}");
                    $model = new WechatTemplateModel();
                    $template = $model->getTemplate($command->getKey());
                    if ($template !== false) {
                        $message = new BooleanCommand(HttpStatus::SUCCESS, null, $command->getOpaque());
                        if (MegaWechatServer::$templateTable->exist($template['tmpl_key'])) {
                            MegaWechatServer::$templateTable->set($template['tmpl_key'], ['tmpl' => $template['template']]);
                        } else {
                            if (count(MegaWechatServer::$templateTable) < Config::get('server.table_size')) {
                                MegaWechatServer::$templateTable->set($template['tmpl_key'], ['tmpl' => $template['template']]);
                            } else {
                                $message = new BooleanCommand(HttpStatus::BAD_REQUEST, 'over table size', $command->getOpaque());
                            }
                        }
                    } else {
                        $message = new BooleanCommand(HttpStatus::BAD_REQUEST, 'template key not exists', $command->getOpaque());
                    }
                } catch (\Exception $ex) {
                    $message = new BooleanCommand(HttpStatus::INTERNAL_SERVER_ERROR, $ex->getMessage(), $command->getOpaque());
                    $this->logger->error(__LINE__ . ' SetTableCommand ' . $ex->getMessage());
                }
                $server->send($command->getFd(), $this->encode($message));
                if ($message->getCode() === HttpStatus::INTERNAL_SERVER_ERROR) {
                    $server->close($command->getFd());
                }
            }

            //调用TASk,执行队列发送消息
            if (MegaWechatServer::$taskActiveNum->get() < $this->taskWorkerNum) {
                MegaWechatServer::$taskActiveNum->add(1);
                $server->task($this->queue->pop());
            }

        } catch (CommandException $ex) {
            $message = new BooleanCommand(HttpStatus::INTERNAL_SERVER_ERROR, $ex->getMessage(), null);
            $this->logger->warning($ex->getMessage(), $server->connection_info($fd, -1, true));
            $server->send($fd, $this->encode($message));
            $server->close($fd);
        }
    }

    function onShutdown(\swoole_server $server, $workerId)
    {
        if (!$server->taskworker) {
            if ($this->queue) {
                $this->queue->close();
            }
        }
    }

    function onTask(\swoole_server $server, $taskId, $fromId, $data)
    {
        $command = unserialize($data);
        if ($command instanceof SendCommand or $command instanceof PushCommand) {
            try {
                //实例化微信对象
                $sn = $command->getApplication();
                $app_id_key = Config::get('wechat.app_id'.$sn);
                $app_secret_key = Config::get('wechat.app_secret'.$sn);
                $this->wechatApi = new WechatApi($app_id_key, $app_secret_key, Config::get('wechat.token'));
                $template = new Template();
                $template->setAppid($app_id_key);
                $template->setOpenid($command->getOpenId());
                $template->setData($command->getData()['template']);
                $template->setNoticeType($command->getData()['notice_type']);
                $template->setIsSend($command->getData()['is_send']);
                $template->setSms($command->getData()['sms']);
                $template->setCompanyNo($command->getData()['company_no']);
                $templateData = MegaWechatServer::$templateTable->get($command->getKey())['tmpl'];
//                $templateData = json_decode($templateData,true);
//                $templateData = $this->searchArray($templateData,'company_no',$template->getCompanyNo());
//                $templateData = json_encode($templateData[0]);
                $template->setTemplate($templateData);

                $template = $template->parse();
                $result = $this->wechatApi->sendMessage($template,false, $command->getData()['is_send'],$command->getData()['notice_type'],$command->getData()['sms']);
                // SendCommand需要响应调用API接口请求
                if ($command instanceof SendCommand) {
                    //$result = 1; //测试使用
                    if ($result['errcode'] === 0) {
                        $message = new BooleanCommand(HttpStatus::SUCCESS, null, $command->getOpaque());
                    } else {
                        $this->logger->info(__LINE__ . ' sendTemplateMessage fail', $result);
                        $result['openid'] = $command->getOpenId();
                        $message = new BooleanCommand(HttpStatus::BAD_REQUEST, json_encode($result), $command->getOpaque());
                    }
                    $server->send($command->getFd(), $this->encode($message));
                }
            } catch (\Exception $ex) {
                $this->logger->error(__LINE__ . ' SendCommand ' . $ex->getMessage());
                $message = new BooleanCommand(HttpStatus::INTERNAL_SERVER_ERROR, $ex->getMessage(), $command->getOpaque());
                $server->send($command->getFd(), $this->encode($message));
                $server->close($command->getFd());
            }
        }
        MegaWechatServer::$taskActiveNum->sub(1);
        $server->finish("finish");
    }

//二维数组查找某一个值，并返回
    public function searchArray($array, $key1, $value1, $key2 = "", $value2 = "")
    {
        $arr = array();
        foreach ($array as $keyp => $valuep) {
            if (empty($key2) || empty($value2)) {
                if ($valuep[$key1] == $value1) {
                    array_push($arr, $valuep);
                }
            } else if (empty($key1) || empty($value1)) {
                if ($valuep[$key2] == $value2) {
                    array_push($arr, $valuep);
                }
            } else {
                if ($valuep[$key1] == $value1 && $valuep[$key2] == $value2) {
                    array_push($arr, $valuep);
                }
            }
        }
        return $arr;
    }

    /**
     * @param $server \swoole_server
     * @param $taskId
     * @param $data
     */
    function onFinish(\swoole_server $server, $taskId, $data)
    {
        if (MegaWechatServer::$taskActiveNum->get() < $this->taskWorkerNum) {
            if (($message = $this->queue->pop()) !== null) {
                MegaWechatServer::$taskActiveNum->add(1);
                $server->task($message);
            }
        }
    }

    function onRequest($request, $response)
    {
        throw new \Exception('not implement onRequest method');
    }

    function onClose(\swoole_server $server, $client_id, $from_id)
    {
        //echo "Client@[{$client_id}:{$from_id}] close" . PHP_EOL;
        $this->logger->info("Client@[{$client_id}:{$from_id}] close");
    }

    function onTimer(\swoole_server $server, $interval)
    {
        throw new \Exception('not implement onTimer method');
    }

}