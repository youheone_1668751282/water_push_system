<?php
/**
 * Created by PhpStorm.
 * User: rain1
 * Date: 2016/3/15
 * Time: 11:52
 */

namespace Wechat;


class Template
{
    private $openid;
    private $data;
    private $template;
    private $appid;
    private $pagepath;
    private $sms;
    private $notice_type;
    protected $company_no;
    protected $is_send;

    /**
     * @return mixed
     */
    public function getAppid()
    {
        return $this->appid;
    }

    /**
     * @param mixed $appid
     */
    public function setAppid($appid)
    {
        if (!is_string($appid)) {
            throw new \InvalidArgumentException('appid must be string');
        }
        $this->appid = $appid;
    }

    /**
     * @return mixed
     */
    public function getPagepath()
    {
        return $this->pagepath;
    }

    /**
     * @param mixed $pagepath
     */
    public function setPagepath($pagepath = '')
    {
        $this->pagepath = $pagepath;
    }


    /**
     * @return mixed
     */
    public function getOpenid()
    {
        return $this->openid;
    }

    /**
     * @param mixed $openid
     */
    public function setOpenid($openid)
    {
        if (!is_string($openid)) {
            throw new \InvalidArgumentException('openid must be string');
        }
        $this->openid = $openid;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        if (!is_array($data) && $data !== null) {
            throw new \InvalidArgumentException('data must be array');
        }
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        if (!is_string($template)) {
            throw new \InvalidArgumentException('template must be json string');
        }
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getSms()
    {
        return $this->sms;
    }

    /**
     * @param mixed
     */
    public function setSms($sms)
    {
        $this->sms = $sms;
    }

    /**
     * @return mixed
     */
    public function getNoticeType()
    {
        return $this->notice_type;
    }

    /**
     * @param mixed
     */
    public function setNoticeType($notice_type)
    {
        $this->notice_type = $notice_type;
    }

    /**
     * @return mixed
     */
    public function getCompanyNo()
    {
        return $this->company_no;
    }

    /**
     * @param mixed
     */
    public function setCompanyNo($company_no)
    {
        $this->company_no = $company_no;
    }

    /**
     * @return mixed
     */
    public function getIsSend()
    {
        return $this->is_send;
    }

    /**
     * @param mixed
     */
    public function setIsSend($is_send)
    {
        $this->is_send = $is_send;
    }


    /**
     * 解析template数据
     * @return string
     */
    public function parse()
    {
        $pattern = [
            '/\\$\\{OPENID\\}/',
        ];
        $replacement = [
            $this->openid,
        ];
        $pattern[] = '/\\$\\{appid\\}/';
        $replacement[] = $this->appid;

        if ($this->data !== null) {
            $pattern[] = '/\\$\\{pagepath\\}/';
            $replacement[] = $this->data['pagepath'];
            foreach ($this->data['data'] as $key => $value) {
                $pattern[] = '/\\$\\{' . $key . '\\}/';
                $replacement[] = '"'.$value['value'].'"';
            }
        }
        $result = preg_replace($pattern, $replacement, $this->template);
        return $result;
    }
}