<?php

/**
 * Created by PhpStorm.
 * User: rain1
 * Date: 2016/6/2
 * Time: 10:53
 */
include dirname(__DIR__) . "/autoload.php";


$client = new \swoole_client(SWOOLE_TCP, SWOOLE_SYNC);
$client->set(array(
    'open_length_check' => true, //打开EOF检测
    'package_length_type' => 'N', //设置EOF
    'package_length_offset' => 0,
    'package_body_offset' => 4,
));
$client->connect('127.0.0.1', 9501, 3);
$data = [
    "first"=> [
        "value"=> "设备异常通知！",
    ],
    "keyword1"=> [
        "value"=> "Ab123456",
    ],
    "keyword2"=> [
        "value"=> "重庆渝北区",
    ],
    "keyword3"=> [
        "value"=> "2018年12月10日",
    ],
    "keyword4"=> [
        "value"=> "离线",
    ],
    "remark"=> [
        "value"=> "欢迎再次使用！",
    ]
];
//合伙人端
$command = new \Network\PushCommand(1, 'oKDUE5hrXAvgGsrzNsDXNBsW6iAA', 'AB123459',$data,'0');
$message = $command->encode();
$client->send($message);
$ret = $client->recv();
$decoder = new \Network\MegaWechatDecoder();
$result = $decoder->decode($ret);
var_dump($result);
//设备管理端
$command = new \Network\PushCommand(1, 'o6IK05Jj_KcFJJ7NQ8-L4-BcBWpw', 'AB123459',$data,'1');
$message = $command->encode();
$client->send($message);
$ret = $client->recv();
$decoder = new \Network\MegaWechatDecoder();
$result = $decoder->decode($ret);
var_dump($result);
